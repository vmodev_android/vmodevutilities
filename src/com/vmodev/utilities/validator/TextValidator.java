/**
Leo
 */
package com.vmodev.utilities.validator;

import com.vmodev.utilities.StringUtils;

import android.widget.EditText;

public class TextValidator {
	public static String notEmpty(EditText editText, String msg) {
		if (editText != null) {
			String text = editText.getText().toString();
			if (!StringUtils.isEmpty(text)) {
				editText.setError(null);
				return text;
			}
			if (msg != null) {
				editText.setError(msg);
			}
		}
		return null;
	}

	public static Double notNegativeDouble(EditText editText, String msg) {
		if (editText != null) {
			String text = editText.getText().toString();
			if (!StringUtils.isEmpty(text)) {
				Double value = null;
				try {
					value = Double.valueOf(text);
				} catch (Exception e) {
					// TODO: handle exception
				}
				if (value != null && value >= 0) {
					editText.setError(null);
					return value;
				}
			}
		}
		if (msg != null) {
			editText.setError(msg);
		}
		return null;
	}
}
