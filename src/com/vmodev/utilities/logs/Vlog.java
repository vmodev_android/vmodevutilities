/**
Leo
 */
package com.vmodev.utilities.logs;

import android.util.Log;

/**
 * Handle every bug and send them to server
 * 
 * @author Leo
 * 
 */
public class Vlog {
	public static void i(String tag, String message) {
		Log.i(tag, message);
	}

	public static void d(String tag, String message) {
		Log.d(tag, message);
	}

	public static void e(String tag, String message) {
		Log.e(tag, message);
	}

	public static void v(String tag, String message) {
		Log.v(tag, message);
	}

}
