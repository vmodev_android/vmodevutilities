/**
Leo
 */
package com.vmodev.utilities.localmessage;

public interface OnReceiveMessage {
	public void onReceiveMessage(String action, LocalMessage message);
}
