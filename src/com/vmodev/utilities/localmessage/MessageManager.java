/**
Leo
 */
package com.vmodev.utilities.localmessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.vmodev.utilities.logs.Vlog;

import android.os.AsyncTask;
import android.util.Log;

public class MessageManager implements ILocalMessageService {
	private final String TAG = MessagesService.class.getSimpleName();
	private static MessageManager instant = new MessageManager();
	private MessagesService service = new MessagesService();

	private synchronized MessagesService getService() {
		return service;
	}

	public static MessageManager getManager() {
		return instant;
	}

	private MessageManager() {

	}

	@Override
	public void sendMessage(final String action, final LocalMessage message) {
		new AsyncTask<Void, Void, List<OnReceiveMessage>>() {
			@Override
			protected List<OnReceiveMessage> doInBackground(Void... params) {
				List<OnReceiveMessage> list = getService().getReceiver(action);
				return list;
			}

			protected void onPostExecute(List<OnReceiveMessage> list) {
				if (list != null) {
					for (OnReceiveMessage onReceiveMessage : list) {
						onReceiveMessage.onReceiveMessage(action, message);
					}
					Log.i(TAG, "Executed " + list.size() + " handler message");
				}
			};
		}.execute();
	}

	@Override
	public void register(final String action,
			final OnReceiveMessage onReceiveMessage) {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				getService().register(action, onReceiveMessage);
				return null;
			}
		};
	}

	@Override
	public void unregister(final String action,
			final OnReceiveMessage onReceiveMessage) {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				getService().unregister(action, onReceiveMessage);
				return null;
			}
		};
	}

	@Override
	public void cleanAll() {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				getService().cleanAll();
				return null;
			}
		};
	}

	private class MessagesService implements ILocalMessageService {
		private Map<String, List<OnReceiveMessage>> mapMessage;

		private MessagesService() {
			mapMessage = new HashMap<String, List<OnReceiveMessage>>();
		}

		public List<OnReceiveMessage> getReceiver(String action) {
			List<OnReceiveMessage> list = null;
			if (mapMessage.containsKey(action)) {
				list = mapMessage.get(action);
			}
			return list;
		}

		@Override
		public void sendMessage(String action, LocalMessage message) {

		}

		@Override
		public void register(String action, OnReceiveMessage onReceiveMessage) {
			// list of receiver
			List<OnReceiveMessage> list = getReceiver(action);
			if (list == null) {
				list = new ArrayList<OnReceiveMessage>();
				mapMessage.put(action, list);
			}
			// add onReceiveMessage to list if it has not added
			if (!list.contains(onReceiveMessage)) {
				list.add(onReceiveMessage);
				Log.i(TAG, "Register a receiver for " + action);
			}
		}

		@Override
		public void unregister(String action, OnReceiveMessage onReceiveMessage) {
			// list of receiver
			List<OnReceiveMessage> list = getReceiver(action);
			if (list != null) {
				for (Iterator iterator = list.iterator(); iterator.hasNext();) {
					OnReceiveMessage onReceive = (OnReceiveMessage) iterator
							.next();
					if (onReceive == onReceiveMessage) {
						iterator.remove();
						Log.i(TAG, "Unregister a receiver for " + action);
					}
				}
			}
		}

		@Override
		public void cleanAll() {
			mapMessage.clear();
		}
	}
}
