/**
Leo
 */
package com.vmodev.utilities;

public class StringUtils {
	public static boolean isNull(Object obj) {
		return obj == null;
	}

	public static boolean isEmpty(String str) {
		return str == null || str.equals("");
	}

	public static boolean isEmpty(CharSequence str) {
		return str == null || str.length() == 0;
	}

	// son.le
	public static String toUpperCaseFirstChar(String strToParse) {
		if (strToParse != null && !strToParse.isEmpty()) {
			char[] stringArray = strToParse.toCharArray();
			stringArray[0] = Character.toUpperCase(stringArray[0]);
			strToParse = new String(stringArray);
		} else {
			strToParse = "";
		}
		return strToParse;
	}
}
