/**
Leo
 */
package com.vmodev.utilities;

import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

public class ViewUtils {

	/**********
	 * spinner
	 */
	public static ArrayAdapter<CharSequence> setAdapter(Context context,
			Spinner spinner, String[] str) {
		return setAdapter(context, spinner,
				android.R.layout.simple_spinner_item,
				android.R.layout.simple_spinner_dropdown_item, str);
	}

	public static ArrayAdapter<CharSequence> setAdapter(Context context,
			Spinner spinner, List<CharSequence> str) {
		return setAdapter(context, spinner,
				android.R.layout.simple_spinner_item,
				android.R.layout.simple_spinner_dropdown_item, str);
	}

	public static ArrayAdapter<CharSequence> setAdapter(Context context,
			Spinner spinner, int textBoxLayoutId, int itemLayoutId,
			List<CharSequence> str) {
		ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(
				context, textBoxLayoutId, str);
		adapter.setDropDownViewResource(itemLayoutId);
		spinner.setAdapter(adapter);
		return adapter;
	}

	public static ArrayAdapter<CharSequence> setAdapter(Context context,
			Spinner spinner, int textBoxLayoutId, int itemLayoutId, String[] str) {
		ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(
				context, textBoxLayoutId, str);
		adapter.setDropDownViewResource(itemLayoutId);
		spinner.setAdapter(adapter);
		return adapter;
	}

	public static <E> ArrayAdapter<E> setObjectAdapter(Context context,
			Spinner spinner, int textBoxLayoutId, int itemLayoutId, List<E> str) {
		ArrayAdapter<E> adapter = new ArrayAdapter<E>(context, textBoxLayoutId,
				str);
		adapter.setDropDownViewResource(itemLayoutId);
		spinner.setAdapter(adapter);
		return adapter;
	}

	/******************
	 * dialog
	 */
	public static void showConfirmDialog(Context context, String title,
			String strBtnOk, String strBtnCancel, Integer iconId,
			DialogInterface.OnClickListener onPositiveButton,
			DialogInterface.OnClickListener onCancelButton) {
		iconId = iconId == null ? android.R.attr.alertDialogIcon : iconId;
		title = title == null ? "Are you sure?" : title;
		strBtnOk = strBtnOk == null ? "Ok" : strBtnOk;
		strBtnCancel = strBtnCancel == null ? "Cancel" : strBtnCancel;
		new AlertDialog.Builder(context).setIconAttribute(iconId)
				.setTitle(title).setPositiveButton(strBtnOk, onPositiveButton)
				.setNegativeButton(strBtnCancel, onCancelButton).create()
				.show();
	}

	public static void showNotifyDialog(Context context, String title) {
		new AlertDialog.Builder(context).setTitle(title)
				.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				}).show();
	}

	public static void showConfirmDialog(Context context, String title,
			DialogInterface.OnClickListener onPositiveButton,
			DialogInterface.OnClickListener onCancelButton) {
		showConfirmDialog(context, title, null, null, null, onPositiveButton,
				onCancelButton);
	}

	public static void showConfirmDialog(Context context, String title,
			DialogInterface.OnClickListener onPositiveButton) {
		showConfirmDialog(context, title, null, null, null, onPositiveButton,
				null);
	}

	/************
	 * Tabs
	 */
	// change color of the tab title
	public static void setTextColorForTabs(TabWidget tabs, int color,
			Context context) {
		for (int index = 0; index < tabs.getChildCount(); index++) {
			TextView tvTab = (TextView) tabs.getChildAt(index).findViewById(
					android.R.id.title);
			tvTab.setTextColor(context.getResources().getColor(color));
		}
	}

	// change color of the tab title
	public static void setColorForTabs(TabWidget tabs, int titleColor,
			int bgColor, Context context) {
		for (int index = 0; index < tabs.getChildCount(); index++) {
			TextView tvTab = (TextView) tabs.getChildAt(index).findViewById(
					android.R.id.title);
			tvTab.setTextColor(context.getResources().getColor(titleColor));
			tabs.getChildAt(index).setBackgroundResource(bgColor);
		}
	}

	/**************
	 * Multimedia
	 */
	/**
	 * add alpha value to rgb color
	 */
	public static int setOpacityRGB(int alpha, int rgb) {
		int red = (rgb >> 16) & 0xFF;
		int green = (rgb >> 8) & 0xFF;
		int blue = rgb & 0xFF;
		return Color.argb(alpha, red, green, blue);
	}

	/**
	 * play a res sound
	 * 
	 * @param context
	 * @param idSound
	 */
	public static void playASound(Context context, int idSound) {
		MediaPlayer mp = MediaPlayer.create(context, idSound);
		mp.setOnVideoSizeChangedListener(new OnVideoSizeChangedListener() {
			@Override
			public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {

			}
		});
		mp.setOnPreparedListener(new OnPreparedListener() {
			@Override
			public void onPrepared(MediaPlayer mp) {

			}
		});
		mp.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				mp.release();
			}
		});
		mp.start();
	}

	public static void showTextView(View view, String str) {
		TextView tv = (TextView) view;
		tv.setText(str);
	}

	public static void hideKeyBoard(Context context, View view) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

	/**
	 * Toast message
	 */
	public static void showToast(Context context, String message, int second) {
		Toast.makeText(context, message, second * 1000).show();
	}

	public static void showToast(Context context, String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	public static void showToast(Context context, int idString) {
		String message = context.getString(idString);
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	public static void showToast(Fragment fragment, String message) {
		if (fragment != null) {
			Toast.makeText(fragment.getActivity(), message, Toast.LENGTH_SHORT)
					.show();
		}
	}

	public static void showToast(Fragment fragment, int idString) {
		if (fragment != null) {
			String message = fragment.getActivity().getString(idString);
			Toast.makeText(fragment.getActivity(), message, Toast.LENGTH_SHORT)
					.show();
		}
	}
	/***
	 * 
	 */
}
