/**
Leo
 */
package com.vmodev.utilities;

public class NumberUtil {
	private static final String TAG = NumberUtil.class.getSimpleName();

	public static double doubleOf(String str, double defaultValue) {
		double value = defaultValue;
		try {
			value = Double.valueOf(str);
		} catch (Exception e) {
			// Vlog.i(TAG, "Can't not convert " + str + " to double");
		}
		return value;
	}

	/**
	 * generate a number text, sample 4567 with start=4 and end=7
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public static String generateTextNumber(int start, int end) {
		String str = "";
		for (int i = start; i <= end; i++) {
			str += String.valueOf(i);
		}
		return str;
	}
}